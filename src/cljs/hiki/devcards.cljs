(ns hiki.devcards
  (:require
   [devcards.core :as devcards]
   [fulcro.client.cards :refer [defcard-fulcro]]
   [hiki.orchestration :as hiki]))


(defcard-fulcro Root
  "This is the root component of hiki - it is what the user will first see upon app start"
  hiki/Root
  {}
  {:inspect-data true})


(defn main []
  (devcards/start-devcard-ui!))
