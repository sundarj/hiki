(ns hiki.orchestration
  "Contains the ui components and orchestrates the app"
  (:require
   [fulcro.client.core :as fulcro]
   [fulcro.client.mutations :as fulcro.mutations]
   [om.next :as om]
   [sablono.core :as sablono]))


(enable-console-print!)


(om/defui ^:once Home
  static fulcro/InitialAppState
  (initial-state [_ _]
    {:ui/channel-ident nil})
  static om/IQuery
  (query [_]
    [:ui/channel-ident])
  static om/Ident
  (ident [_ _]
    [:components/by-name `Home])
  Object
  (render [instance]
    (let [{:keys [ui/channel-ident] :as props} (om/props instance)]
      (sablono/html
       [:form
        {:method "post"
         :on-submit (fn submit-handler [event]
                      (.preventDefault event)
                      (om/transact! instance `[(set-channel-ident {:channel-ident ~channel-ident}) ::channel-ident
                                               (fulcro.mutations/set-props {:ui/channel-ident nil})]))}
        [:label {:for "channel-ident"}
         "YouTube channel identifier*"]
        [:input
         {:name "channel-ident"
          :id "channel-ident"
          :value (str channel-ident)
          :placeholder "e.g UC-9-kyTW8ZkZNDHQJ6FgpwQ"
          :required true
          :on-change (fn change-handler [event]
                       (fulcro.mutations/set-string! instance :ui/channel-ident :event event))}]
        [:button {:type "submit"}
         "Get playlists"]]))))


(def ui-home (om/factory Home))


(om/defui ^:once App
  static fulcro/InitialAppState
  (initial-state [_ _]
    {::home (fulcro/get-initial-state Home nil)
     ::channel-ident nil
     ::title "hiki"})
  static om/IQuery
  (query [_]
    [::channel-ident ::title {::home (om/get-query Home)}])
  static om/Ident
  (ident [_ _]
    [:components/by-name `App])
  Object
  (render [instance]
    (let [{::keys [channel-ident title home]} (om/props instance)]
      (sablono/html
       [:div
        [:h1 title]
        (when-not channel-ident
          (ui-home home))]))))


(def ui-app (om/factory App))


(om/defui ^:once Root
 static fulcro/InitialAppState
 (initial-state [_ _]
   {:ui/react-key (random-uuid)
    :app (fulcro/get-initial-state App nil)})
 static om/IQuery
 (query [_]
   [:ui/react-key
    {:app (om/get-query App)}])
 Object
 (render [instance]
   (let [{:keys [ui/react-key app]} (om/props instance)]
     (sablono/html
      [:div {:key react-key}
       (ui-app app)]))))


(fulcro.mutations/defmutation set-channel-ident
  "Set the app's YouTube channel ident"
  [{:keys [channel-ident] :as params}]
  (action [{:keys [state] :as env}]
    (swap! state assoc-in (conj (om/ident App nil) ::channel-ident) channel-ident)))


(defonce app (atom (fulcro/new-fulcro-client)))


(defn refresh []
  (swap! app fulcro/mount Root "app"))


(defn main []
  (refresh))