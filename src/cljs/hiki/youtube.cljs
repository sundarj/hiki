(ns hiki.youtube
  "YouTube API functions"
  (:require
   [cljs.core.async :as async]
   [cljs-http.client :as http])
  (:require-macros
   [cljs.core.async.macros :refer [go]]))


(enable-console-print!)


(def api-key "AIzaSyAtZLyuduA-5z8YsQuUj3Pdkx7OYhI3L38")


(defn playlists [id]
  (go
    (let [query-params {"key" api-key
                        "part" "snippet"
                        "fields" "items(id,snippet(title))"
                        "channelId" id}
          request (http/get "https://www.googleapis.com/youtube/v3/playlists"
                            {:query-params query-params})
          {:keys [success body] :as response} (async/<! request)]
      (if success
        (:items body)
        (throw
          (ex-info "request failed" response))))))

