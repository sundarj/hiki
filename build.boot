(set-env!
  :source-paths #{"src/cljs"}
  :resource-paths #{"src/html"}

  :dependencies '[;; app deps
                  [devcards "0.2.3"]
                  [fulcrologic/fulcro "1.0.0"]
                  [org.clojure/clojure "1.9.0-RC1"]
                  [org.clojure/clojurescript "1.9.946"]
                  [org.omcljs/om "1.0.0-beta1"]
                  [sablono "0.8.1"]

                  ;; boot tasks
                  [adzerk/boot-cljs "2.1.4" :scope "test"]
                  [adzerk/boot-cljs-repl "0.3.3" :scope "test"]
                  [adzerk/boot-reload "0.5.2" :scope "test"]
                  [binaryage/devtools "0.9.7" :scope "test"]
                  [binaryage/dirac "RELEASE" :scope "test"]
                  [com.cemerick/piggieback "0.2.2" :scope "test"]
                  [org.clojure/tools.nrepl "0.2.13" :scope "test"]
                  [pandeiro/boot-http "0.8.3" :scope "test"]
                  [powerlaces/boot-cljs-devtools "0.2.0" :scope "test"]
                  [weasel "0.7.0" :scope "test"]])
                  
                  

(require '[adzerk.boot-cljs :refer [cljs]]
         ;'[adzerk.boot-cljs-repl :refer [cljs-repl start-repl]]
         '[adzerk.boot-reload :refer [reload]]
         '[pandeiro.boot-http :refer [serve]]
         '[powerlaces.boot-cljs-devtools :refer [cljs-devtools dirac]])
         

(deftask build-js
  "Build javascript files"
  []
  (comp
    (cljs
      :source-map true
      :compiler-options {:devcards true})
    (target :dir #{"target"})))

(deftask dev
  "Launch development environment"
  []
  (comp
    (serve :dir "target")
    (watch)
    (reload)
    ;(cljs-repl)
    (cljs-devtools)
    (dirac)
    (build-js)))

(deftask dev-codenvy
  "Launch codenvy-friendly development environment"
  []
  (comp
    (watch)
    (build-js)))
